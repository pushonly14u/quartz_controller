package com.example.demo;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

@Service
public class JobService {

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;

    public void myScheduler(String name){

        JobDetail jobDetail =
             newJob().ofType(SampleJob.class).storeDurably().withIdentity("job_1","job_group_1").withDescription("Invoke Sample Job service...").usingJobData("name",name+" by Job_1").build();

        Trigger trigger =
             newTrigger().forJob(jobDetail).withIdentity("trigger_1","trigger_group_1").withDescription("Sample trigger").withSchedule(simpleSchedule().withIntervalInSeconds(2).repeatForever()).build();

        JobDetail jobDetail2 =
                newJob().ofType(SampleJob.class).storeDurably().withIdentity(JobKey.jobKey("job_key_2")).withIdentity("job_2","job_group_2").usingJobData("name",name+" by Job_2").withDescription("Invoke Sample Job service... 2").build();

        Trigger trigger2 =
                newTrigger().forJob(jobDetail2).withIdentity(TriggerKey.triggerKey("trigger_key_2")).withIdentity("trigger_2","trigger_group_2").withDescription("Sample trigger 2").withSchedule(CronScheduleBuilder.cronSchedule("* * * ? * *")).build();

        try {
            Scheduler scheduler =  schedulerFactoryBean.getScheduler();
            scheduler.scheduleJob(jobDetail, trigger);
            scheduler.scheduleJob(jobDetail2, trigger2);

        } catch (SchedulerException e) {
            e.printStackTrace();
        }

    }
}