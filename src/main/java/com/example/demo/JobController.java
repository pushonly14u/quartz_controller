package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JobController {

    @Autowired
    public JobService jobService;

    @GetMapping("/trig")
    public String t1(@RequestParam String name){
        jobService.myScheduler(name);
        return "You job is Scheduled\nScheduler will pick it up and start Execution";
    }
}
